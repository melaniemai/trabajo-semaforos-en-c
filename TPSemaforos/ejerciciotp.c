#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>

sem_t sal;
sem_t plancha;
sem_t hornear;
pthread_mutex_t ganador;

int equipoGanador = -1;

void escribirArchivo(char msg[], int equipo);

void cortarCebollaAjoyPerejil(int equipo)
{
        escribirArchivo("Equipo %d: cortando cebolla, ajo y perejil\n",equipo);
        usleep(1000000);
        escribirArchivo("Equipo %d: terminé de cortar cebolla, ajo y perejil\n\n", equipo);
}

void mezclarConCarnePicada(int equipo)
{
        escribirArchivo("Equipo %d: mezclando con la carne!\n", equipo);
        usleep(1000000);
        escribirArchivo("Equipo %d: listo! falta salar!\n\n", equipo);
}

void salar(int equipo)
{
	sem_wait(&sal);
        escribirArchivo("Equipo %d: salando la carne!\n", equipo);
        usleep(2000000);
        escribirArchivo("Equipo %d: carne condimentada!\n\n", equipo);
	sem_post(&sal);
}

void calentarPan(int equipo)
{
	sem_wait(&hornear);
        escribirArchivo("Equipo %d: calentando pan\n", equipo);
        usleep(4000000);
        escribirArchivo("Equipo %d: pan caliente!\n\n", equipo);
	sem_post(&hornear);
}

void armarMedallones(int equipo)
{
        escribirArchivo("Equipo %d: armando los medallones\n", equipo);
        usleep(1000000);
        escribirArchivo("Equipo %d: medallones listos para cocinar!\n\n", equipo);
}

void cocinarMedallones(int equipo)
{
	sem_wait(&plancha); //
        escribirArchivo("Equipo %d: cocinando medallon!\n", equipo);
        usleep(2000000);
        escribirArchivo("Equipo %d: medallon cocido!\n\n", equipo);
	sem_post(&plancha);
}

void cortarLechugayTomate(int equipo)
{
        escribirArchivo("Equipo %d: cortando tomate y lechuga!\n", equipo);
        usleep(1000000);
        escribirArchivo("Equipo %d: terminé de cortar tomate y lechuga\n\n", equipo);
}

void armarHamburguesas(int equipo)
{
	escribirArchivo("\nEquipo %d: preparando las hamburguesas!\n", equipo);
        usleep(2000000);
        escribirArchivo("\nEquipo %d: terminé! 2 hamburguesas listas!\n", equipo);
        pthread_mutex_lock(&ganador); //el primero que entra lockea
	if (equipoGanador == -1) //guarda el primero que entro, solo uno va a entrar al if
	{
		equipoGanador = equipo;
	        escribirArchivo("\n********************************\n Equipo %d: primero en entregar!\n********************************\n\n", equipo);
		printf("El equipo %d ya presentó las dos hamburguesas!\n\n", equipo);
	}
	pthread_mutex_unlock(&ganador); //el primero que entra deslockea para que los otros dos puedan terminar
}

void escribirArchivo(char msg[], int equipo)
{
        FILE* fichero;
	fichero = fopen("registroGanadorHamburguesas.txt", "a");
	fprintf (fichero,msg,equipo);
	fclose(fichero);
}

void *cocinarConReceta(void* numero)
{
	int equipo = *(int*)numero;  //Casteo de puntero a un entero
	char linea[40];
        FILE *receta;
        receta = fopen("receta1.txt", "r");
	for (int i=0; i<11;  i++) {
		fgets (linea, 40,  (FILE*) receta);
        	if (strstr(linea, "cebolla") != NULL) //si la linea contiene esas palabras
		{
			cortarCebollaAjoyPerejil(equipo);
       		}
		if (strstr(linea,"mezclar") != NULL)
        	{
		        mezclarConCarnePicada(equipo);
        	}
		if (strstr(linea, "salar") != NULL)
        	{
        	        salar(equipo);
        	}
		if (strcmp("armar medallones\n", linea) == 0)
        	{
        	        armarMedallones(equipo);
			printf("El equipo %d tiene sus medallones listos, ¡falta cocinarlos!\n\n", equipo);
        	}
		if (strstr(linea, "pan") != NULL)
        	{
        	        calentarPan(equipo);
        	}
        	if (strstr(linea, "lechuga") != NULL)
        	{
			cortarLechugayTomate(equipo);
        	}
        	if (strstr(linea, "cocinar") != NULL) // lo unico que se cocina son los medallones, se sobreentiende
        	{
			cocinarMedallones(equipo);
        	}
        	if (strstr(linea, "hamburguesas") != NULL)
        	{
			printf("El equipo %d está armando las hamburguesas!\n\n", equipo);
        	        armarHamburguesas(equipo);
        	}
	}
        pthread_exit(0);
}

void *limpiarArchivo()
{
        FILE *fichero;
        char filename[] = "registroGanadorHamburguesas.txt";
        fichero = fopen(filename, "w");
        int elimino = remove (filename);
}

int main()
{
	limpiarArchivo();

        char linea[40];
        FILE *fich1;
	fich1 = fopen("receta1.txt", "r");
        for (int i = 0; i < 10; i++)
        {
                fgets (linea, 40,  (FILE*) fich1);
                escribirArchivo(linea,0);
        }

	sem_init(&sal, 0, 1);
	sem_init(&plancha, 0, 1);
	sem_init(&hornear, 0, 1);

	pthread_mutex_init(&ganador, NULL);

        pthread_t t1,t2,t3;
	int equipo1 = 1; int equipo2 = 2; int equipo3 = 3;
	pthread_create(&t1, NULL, cocinarConReceta, &equipo1);
	pthread_create(&t2, NULL, cocinarConReceta, &equipo2);
	pthread_create(&t3, NULL, cocinarConReceta, &equipo3);

        pthread_join(t1, NULL); pthread_join(t2, NULL); pthread_join(t3, NULL);

	sem_destroy(&sal); sem_destroy(&plancha); sem_destroy(&hornear);

	pthread_mutex_destroy(&ganador);

	escribirArchivo("\nEl equipo %d fue el primero en presentar las dos hamburguesas. Es el ganador!\n", equipoGanador);
	printf("Tenemos un ganador!\n\nEl equipo %d fue el primero en presentar las dos hamburguesas!!\n", equipoGanador);

	char cadena[1];
	printf("\nQuiere que le muestre el proceso del concurso?\nResponda s o n\n");
	scanf("%s", cadena);

	if (strcmp(cadena,"s") == 0) //si la respuesta fue (s)i, imprimo el archivo
	{
		char linea[80];
	        FILE *arch;
		printf("\nGenial! El archivo registroGanadorHamburguesas.txt contiene lo siguiente: \n\n");
	        arch = fopen("registroGanadorHamburguesas.txt", "r");
        	while (fgets (linea, 100, (FILE*) arch) != NULL)
        	{
                 	printf("%s", linea);
        	}
	}
	else //cualquier otra cosa que haya contestado, termina
	{
		printf("perfecto! nos vemos :)\n");
	}
	pthread_exit(NULL);
}

